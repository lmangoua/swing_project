/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import employee.BaseClass;
import employee.Employee;
import employee.Manager;
import employee.PersonalAssistant;
import java.util.Scanner;

/**
 *
 * @author lmangoua
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        int _names_size = 0;
        String[] _names = new String[_names_size];

        String _names_item;
        for (int _names_i = 0; _names_i < _names_size; _names_i++) {
            try {
                _names_item = in.nextLine();
            } catch (Exception e) {
                _names_item = null;
            }
            _names[_names_i] = _names_item;
        }

        BaseClass[] employees = new BaseClass[_names_size];

        Manager manager = new Manager("", 0, 0);
        for (int _names_i = 0; _names_i < _names_size; _names_i++) {
            String[] parts = _names[_names_i].split(":");

            switch (parts[0]) {
                case "M":
                    manager = new Manager(parts[1], Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                    System.out.println(manager.getName() + "has an additional bonus of " + manager.getBonus());
                    employees[_names_i] = manager;
                    break;
                case "PA":
                    PersonalAssistant pa = new PersonalAssistant(parts[1], Integer.parseInt(parts[2]), manager);
                    System.out.println(pa.getName() + "'s boss is " + pa.getBossName());
                    employees[_names_i] = pa;
                    break;
                default:
                    try {
                        Employee employee = new Employee(parts[1], Integer.parseInt(parts[2]));
                        employee[_names_i] = employee;
                    } catch (Exception ex) {
                        System.out.println(parts[1] + "'s salary is incorrect. " + ex.getMessage());
                    }
                    break;
            }
        }

        System.out.println("\nPackage before increase");
        for (int i = 0; i < _names_size; i++) {
            if (employees[i] != null) {
                employees[i].printSalaryPackage();
            }
        }

        for (int i = 0; i < _names_size; i++) {
            if (employees[i] != null) {
                employees[i].increaseSalary();
            }
        }

        System.out.println("\nPackage after increase");
        for (int i = 0; i < _names_size; i++) {
            if (employees[i] != null) {
                employees[i].printSalaryPackage();
            }
        }
    }

}
