/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author lmangoua
 */
public class BaseClass {
    
        protected String name;
	protected double salary;
	
	public BaseClass() {
		name = "no name";
		salary = 0.00;
	}
	
	public BaseClass(String n, double s) {
		name = n;
		setSalary(s);
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double s) {
		salary = s;
	}
	
	public String getName() {
		return name;
	}
	
	public void increaseSalary() {
		//
	}
	
	public void printSalaryPackage() {
		System.out.println("Salary of " + name + " is " + salary);
	}
    
}
