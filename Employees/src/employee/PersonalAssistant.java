/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author lmangoua
 */
public class PersonalAssistant extends Employee {
    
    //Instance declaration
    private String name;
    private double salary;
    private Manager manager;
    
    //Constructor
    public PersonalAssistant() {
    }

    public PersonalAssistant(String name, double salary, Manager manager) {
        this.name = name;
        this.salary = salary;
        this.manager = manager;
    }
    
    //Getter & Setter
    public Manager getBossName() {
        return manager;
    }
    
    
}
